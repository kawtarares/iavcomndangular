import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Error404Component } from './core/error404/error404.component';
import { AddUserComponent } from './utilisateurs/add-user/add-user.component';
import { UserListComponent } from './utilisateurs/user-list/user-list.component';
import { AppComponent } from './app.component';
import { HomeComponent } from './core/home/home.component';


const routes: Routes = [
    {path: '', redirectTo: '/home',
    pathMatch: 'full'
    },
    
    {path :'home' , component:HomeComponent}

,    {
        path :'' , component: HomeComponent
    },
    {
       path:'adduser',component:AddUserComponent
    },
    {
        path:'userlist',component:UserListComponent
    },
    { 
      path: '**', component: Error404Component 
    }
    ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }