import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './core/sidebar/sidebar.component';
import { Error404Component } from './core/error404/error404.component';
import { UserListComponent } from './utilisateurs/user-list/user-list.component';
import { AddUserComponent } from './utilisateurs/add-user/add-user.component';
import { HomeComponent } from './core/home/home.component';
import { CommandComponent } from './command/command.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    Error404Component,
    UserListComponent,
    AddUserComponent,
    HomeComponent,
    CommandComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
